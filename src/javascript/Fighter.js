

class Fighter {
    constructor(fighterFullInfo) {
        this.name = fighterFullInfo.name;
        this.health = fighterFullInfo.health;
        this.attack = fighterFullInfo.attack;
        this.defense = fighterFullInfo.defense;
        this.source = fighterFullInfo.source;

    }


    getHitPower(attack) {
        return (attack * Math.floor(Math.random() * 2 + 1));
    }

    getBlockPower(defense) {
        return (defense * Math.floor(Math.random() * 2 + 1));
    }
}

export default Fighter;